import React from "react";
import { StyleSheet, View, Button, Text,Image} from "react-native";

export default class Home extends React.Component {
    
    static navigationOptions = {
        title: '                Bill Bee 🐝',
        headerStyle: {
            backgroundColor: '#67cc31',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: "bold",
            fontSize:30,
           
          },
      };

    render() {
     return (
       <View Style={styles.container}>
           <Image  style={{width: 415, height: 110, marginBottom:10}} 
            source={require("../Screens/image/logo.png")}/>
          
          

           <View  style={styles.selet}>
            <Text style={{fontSize:30,fontWeight:"500"}}>  Select Language ⬇ </Text>
            </View>

            <View style={styles.btn}>
              <Button title="kannada"  color="#a55eea" onPress={()=> {this.props.navigation.navigate("Kannada")}}/>
            </View> 
            
    
            <View style={styles.btn}>
              <Button title="Hindi" color="#0A79DF" onPress={()=> {this.props.navigation.navigate("Hindi");}}/>
            </View>

            <View style={styles.btn}>
              <Button title="Telugu"  color="#1287A5" onPress={()=> {this.props.navigation.navigate("Telugu");}}/>
            </View>

            <View style={styles.btn}>
             <Button title="English" color="orange" onPress={()=> {this.props.navigation.navigate("English");}}/>
            </View>
            
            
      </View>

        );
    }
}




const styles = StyleSheet.create({

    container:{
        flex:1,
        justifyContent:'center',
        backgroundColor:'#fff'
    },

    selet:{
        backgroundColor:'#67cc31',
        height:65,
        margin:20,
        justifyContent:"center",
        alignItems:"center",
        

    },
    btn:{
      paddingTop:30,
      margin:10,
     
    }
});