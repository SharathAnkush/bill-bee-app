import React from "react";
import {View , Text, StyleSheet, Button , ScrollView } from "react-native";

export default class Telugu extends React.Component {

   static navigationOptions = {
      title: '     TELUGU CHANNEL LIST',
      headerStyle: {
         backgroundColor: '#1287A5',
       },
       headerTintColor: '#fff',
       headerTitleStyle: {
         fontWeight: 'bold',
       },
    };

    render() {
        return(
          <ScrollView>
          <View  style={styles.Screen}>
           <Text  style={styles.ChannelTitle}> Choice your Channel</Text>
           
           <View style={styles.cards}>
           <View style={styles.Cardstyle}>
               <Text  style={styles.ck}> Colors Kannada </Text>
               <Text  style={styles.ckp}>₹ 19.00</Text>
            <View  style={styles.btn}>
               <Button title="Remove" color="#f53b57"/>
               <Button title="Add" color="#1287A5"/>
            </View> 
           </View>
          </View>
        
          <View style={styles.cards}>
           <View style={styles.Cardstyle}>
               <Text  style={styles.ck}> Colors Super </Text>
               <Text  style={styles.ckp}>₹ 3.00</Text>
            <View  style={styles.btn}>
               <Button title="Remove" color="#f53b57" />
               <Button title="Add" color="#1287A5"/>
            </View> 
           </View>
         </View>
   
         <View style={styles.cards}>
           <View style={styles.Cardstyle}>
               <Text  style={styles.ck}> Color Cinema </Text>
               <Text  style={styles.ckp}>₹ 2.00</Text>
            <View  style={styles.btn}>
               <Button title="Remove" color="#f53b57" />
               <Button title="Add" color="#1287A5"/>
            </View> 
           </View>
         </View>
   
   
         <View style={styles.cards}>
           <View style={styles.Cardstyle}>
               <Text  style={styles.ck}> Zee Kannada </Text>
               <Text  style={styles.ckp}>₹ 19.00</Text>
            <View  style={styles.btn}>
               <Button title="Remove" color="#f53b57" />
               <Button title="Add" color="#1287A5"/>
            </View> 
           </View>
         </View>
         
         <View style={styles.cards}>
           <View style={styles.Cardstyle}>
               <Text  style={styles.ck}> Udaya Music </Text>
               <Text  style={styles.ckp}>₹ 6.00</Text>
            <View  style={styles.btn}>
               <Button title="Remove" color="#f53b57" />
               <Button title="Add" color="#1287A5"/>
            </View> 
           </View>
         </View>
         
         <View style={styles.cards}>
           <View style={styles.Cardstyle}>
               <Text  style={styles.ck}> Udaya Music </Text>
               <Text  style={styles.ckp}>₹ 6.00</Text>
            <View  style={styles.btn}>
               <Button title="Remove" color="#f53b57" />
               <Button title="Add" color="#1287A5"/>
            </View> 
           </View>
         </View>
         
         <View style={styles.cards}>
           <View style={styles.Cardstyle}>
               <Text  style={styles.ck}> Udaya Music </Text>
               <Text  style={styles.ckp}>₹ 6.00</Text>
            <View  style={styles.btn}>
               <Button title="Remove" color="#f53b57" />
               <Button title="Add" color="#1287A5"/>
            </View> 
           </View>
         </View>
         
         <View style={styles.cards}>
           <View style={styles.Cardstyle}>
               <Text  style={styles.ck}> Udaya Music </Text>
               <Text  style={styles.ckp}>₹ 6.00</Text>
            <View  style={styles.btn}>
               <Button title="Remove" color="#f53b57" />
               <Button title="Add" color="#1287A5"/>
            </View> 
           </View>
         </View>
         
        </View>
       </ScrollView>
     );
   }
  }
   
  const styles = StyleSheet.create({
       Screen:{
           flex:1,
           padding:8,
           alignItems:'center'
   },
   
   ChannelTitle:{
       fontSize:27,
       fontWeight:"600",
       color:"#1287A5"
     },
   
    Cardstyle:{ 
      justifyContent:'center',
      width:900,
      maxWidth:'90%',
      alignItems:'center',
      elevation: 8,
      backgroundColor:'white',
      padding:13,
      borderRadius:10
   
   
    },
   
    ck:{
        fontSize:23,
        fontWeight:"500",
       
    },
   
    ckp:{
        fontSize:23,
        fontWeight:"500"
    },
       
    btn:{
     flexDirection:'row',
     width:'100%',
     justifyContent:'space-between',
     paddingHorizontal:20,
     
   
    },
   
    cards:{
        paddingTop:13
    },
       
   
});
