import  React from "react";
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from "./Navigation/Home";
import Kannada from "./Screens/Kannada";
import Hindi from "./Screens/Hindi";
import Telugu from "./Screens/Telugu";
import English from "./Screens/English";

const MainNavigator =  createStackNavigator({
  
    Home: { screen: Home },
    Kannada: { screen: Kannada },
    Hindi: {screen: Hindi },
    Telugu: {screen: Telugu },
    English: {screen: English }

 },

 {
     defaultNavigationOptions:{
         headerTintColor:"red",
         
     }
 }

);
 

const App =  createAppContainer (MainNavigator);

export default App;

